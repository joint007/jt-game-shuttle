//todo: remake star; add sound effects; add hold click power shoot effect;
const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')
const container = document.getElementById('container')
// const enemyRadiusMax = 35
const enemyRadiusMax = innerWidth > innerHeight ? innerHeight / 15 : innerWidth / 15
const enemyRadiusMin = 30
const shootStep = 15
const myArr = [
    'You are playing a game made from plain js',
    'My phone number is 6472835687',
    'Call me JT for short',
    'Don\'t hesitate'
]
canvas.width = innerWidth
canvas.height = innerHeight
const timeInterval = 1200
let spawnID = null
const instruction = document.getElementById('instruction')
const startBtn = document.getElementById('start')
//for cards
const cardList = document.getElementById('card-list')
const cardListPhone = document.getElementById('card-list-phone')
//------------------for stars--------
const spacingX = innerWidth / 10
const spacingY = innerHeight / 10
const maxStarRadius = 6
const maxStarSpeed = 3
//-----------------------------------
const scoreEle = document.getElementById('scoreEle')
const startGameBtn = document.getElementById('startGameBtn')
const panel = document.getElementById('panel')
const bigScore = document.getElementById('bigScore')
//change progressbar
let changeUnit = 15
let currentWidth = 0
const proBar = document.getElementById('proBar')
//for drawing option (bonus vs regular)
let counter = 0
//for bonus color
let counter2ID = null
let counter2 = 0
let animationId
let score = 0
const friction = 0.99
let boomed = false
let boomTimerID

const backgroundMusic = document.getElementById('backgroundMusic');
const bulletSound = document.getElementById('bulletSound');

// Play the background music
function playMusic(music) {
    music.play();
}

// Pause the background music
function pauseBackgroundMusic(music) {
    music.pause();
}

// Mute the background music
function muteBackgroundMusic(music) {
    music.muted = true;
}

// Unmute the background music
function unmuteBackgroundMusic(music) {
    music.muted = false;
}

// Pause or stop the background music
function stopMusic(music) {
    music.pause();
    music.currentTime = 0; // Reset the playback position to the beginning
}

class Star {
    constructor(x, y, radius, color, speed) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.speed = speed
    }

    draw() {
        c.beginPath();
        c.fillStyle = this.color;
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        c.fill();
    }

    update() {
        this.y += this.speed
        if (this.y > innerHeight) {
            this.y = 0
        }
        this.draw()
    }
}

class Player {
    constructor(x, y, radius, color) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
    }

    draw() {
        c.beginPath();
        const playerImg = new Image()
        playerImg.src = 'ufo2.png'
        c.drawImage(playerImg, this.x - 35, this.y - 40, 75, 75);
    }
}

class Projectile {
    constructor(x, y, radius, color, velocity, isBoom) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
        this.isBoom = isBoom
    }

    draw() {
        c.beginPath()
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        c.fillStyle = this.color
        c.fill()
    }

    update() {
        this.draw()
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y
    }
}

class Enemy {
    constructor(x, y, radius, color, velocity, counter, a, rotationSpeed) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
        this.counter = counter
        this.a = a
        this.rotation = 0
        this.rotationSpeed = rotationSpeed
    }

    draw() {
        c.fillStyle = this.color
        c.save();
        c.translate(this.x, this.y); // Translate to the center of the meteorite
        if (this.counter % 3 === 0) {
            const lockImage = new Image();
            lockImage.src = counter2 % 2 === 0 ? 'lock-1.png' : 'lock-2.png'; // Image source
            c.drawImage(lockImage, -this.radius, -this.radius, this.radius * 1.5, this.radius * 1.5); // Draw the meteorite image
        } else {
            const meteoriteImage = new Image();
            meteoriteImage.src = 'meteorite.png'; // Image source
            c.rotate(this.rotation); // Rotate the canvas
            c.drawImage(meteoriteImage, -this.radius, -this.radius, this.radius * 2, this.radius * 2); // Draw the meteorite image
        }
        c.restore();
    }

    update() {
        this.draw()
        this.x = this.x + this.velocity.x * this.a
        this.y = this.y + this.velocity.y * this.a
        this.rotation += this.rotationSpeed
    }
}

class Particle {
    constructor(x, y, radius, color, velocity) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
        this.alpha = 1
    }

    draw() {
        c.save()
        c.globalAlpha = this.alpha
        c.beginPath()
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        c.fillStyle = this.color
        c.fill()
        c.restore()
    }

    update() {
        this.draw()
        this.velocity.x *= friction
        this.velocity.y *= friction
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y
        this.alpha -= 0.02
    }
}

const x = canvas.width / 2
const y = canvas.height / 2
let player
let projectiles = []
let enemies = []
let particles = []
let stars = []

// Function to generate a random number between min and max
function random(min, max) {
    return Math.random() * (max - min) + min;
}

//Creating starry sky
function randomInt(max) {
    return Math.floor(Math.random() * max);
}

function createStars(width, height, spacingX, spacingY, maxStarRadius, maxStarSpeed) {
    for (let x = 0; x < width; x += spacingX) {
        for (let y = 0; y < height; y += spacingY) {
            stars.push(new Star(x + randomInt(spacingX), y + randomInt(spacingY), randomInt(maxStarRadius) / 2, 'white', randomInt(maxStarSpeed) / 2 + 0.5));
        }
    }
    stars.forEach(function (star) {
        star.draw()
    });
}

createStars(innerWidth, innerHeight, spacingX, spacingY, maxStarRadius, maxStarSpeed);

//*********init game**********
function init() {
    counter = 0
    counter2 = 0
    clearInterval(counter2ID)
    counter2ID = setInterval(() => {
        counter2 += 1
    }, 200)
    player = new Player(x, y, 26, 'white')
    projectiles = []
    enemies = []
    particles = []
    score = 0
    scoreEle.innerHTML = score
    bigScore.innerHTML = score
    proBar.style.width = '0'
    clearInterval(spawnID)
}

function spawnEnemies() {
    spawnID = setInterval(() => {
        const radius = Math.random() * (enemyRadiusMax - enemyRadiusMin) + enemyRadiusMin
        let x
        let y
        if (Math.random() < 0.5) {
            x = Math.random() < 0.5 ? 0 - radius : canvas.width + radius
            y = Math.random() * canvas.height
        } else {
            y = Math.random() < 0.5 ? 0 - radius : canvas.height + radius
            x = Math.random() * canvas.width
        }
        const color = `hsla(${Math.random() * 360}, 50%, 50%, ${Math.random() + 0.5})`
        const angle = Math.atan2(canvas.height / 2 - y,
            canvas.width / 2 - x)
        const velocity = {
            x: Math.cos(angle),
            y: Math.sin(angle)
        }
        counter += 1
        enemies.push(new Enemy(x, y, radius, color, velocity, counter, (randomInt(3) + 1) / 1.5, random(-0.1, 0.1)))
    }, timeInterval)
}

function updateScore(value) {
    score += value
    scoreEle.innerHTML = score
    scoreEle.classList.add('scoreEffect')
    setTimeout(() => {
        scoreEle.classList.remove('scoreEffect')
    }, 100)
}

function updateProBar(isUnlock) {
    if (isUnlock) {
        currentWidth += changeUnit
        proBar.style.width = `${currentWidth}%`
    }
}

function animate() {
    animationId = requestAnimationFrame(animate)
    c.fillStyle = 'rgba(0, 0, 0, 0.1)'
    c.fillRect(0, 0, canvas.width, canvas.height)
    const numStars = 3;
    for (let i = 0; i < numStars; i++) {
        const x = Math.random() * canvas.width;
        const y = Math.random() * canvas.height;
        const radius = Math.random() * 2.5;
        c.fillStyle = '#fff'; // White color for stars
        c.beginPath();
        c.arc(x, y, radius, 0, Math.PI * 2);
        c.fill();
    }
    stars.forEach(function (star) {
        star.update()
    });
    player.draw()
    particles.forEach((particle, index) => {
        if (particle.alpha <= 0) {
            particles.splice(index, 1)
        } else {
            particle.update()
        }
    })
    projectiles.forEach((projectile, projectileIndex) => {
        projectile.update()
        if (projectile.x + projectile.radius < 0 ||
            projectile.x - projectile.radius > canvas.width ||
            projectile.y + projectile.radius < 0 ||
            projectile.y - projectile.radius > canvas.height) {
            setTimeout(() => {
                projectiles.splice(projectileIndex, 1)
            }, 0)
        }
    })
    enemies.forEach((enemy, index) => {
        enemy.update()
        //Game over
        const dist = Math.hypot(player.x - enemy.x, player.y - enemy.y)
        if (dist - enemy.radius - player.radius < 1.5) {
            cancelAnimationFrame(animationId)
            setTimeout(() => {
                panel.style.display = 'flex'
                bigScore.innerHTML = score
            }, 1000)
            removeEventListener('click', shoot)
            if ('PointerEvent' in window) {
                removeEventListener('pointerdown', boom);
            } else {
                removeEventListener('mousedown', boom);
            }
            stopMusic(backgroundMusic)
            currentWidth = 0
            startBtn.innerText = "TRY AGAIN"
        }
        if (currentWidth >= 100) {
            instruction.innerHTML = 'YOU GOT IT!'
            cancelAnimationFrame(animationId)
            setTimeout(() => {
                panel.style.display = 'none'
                cardList.style.opacity = '1'
                cardList.style.zIndex = '0'
                cardListPhone.style.opacity = '1'
                cardListPhone.style.zIndex = '0'
                container.style.opacity = '0.5'
            }, 1000)
            removeEventListener('click', shoot)
            if ('PointerEvent' in window) {
                removeEventListener('pointerup', () => {
                    clearTimeout(boomTimerID)
                })
                removeEventListener('pointerdown', boom);
            } else {
                removeEventListener('mousedown', boom);
                removeEventListener('mouseup', () => {
                    clearTimeout(boomTimerID)
                })
            }
        }
        projectiles.forEach((projectile, projectileIndex) => {
            const dist = Math.hypot(projectile.x - enemy.x, projectile.y - enemy.y)
            //when projections touch enemy
            if (dist - enemy.radius - projectile.radius < 2) {
                //create explosions
                for (let i = 1; i < enemy.radius * 2; i++) {
                    particles.push(
                        new Particle(projectile.x, projectile.y, Math.random() * 2, projectile.color, {
                            x: (Math.random() - 0.5) * (Math.random() * 6),
                            y: (Math.random() - 0.5) * (Math.random() * 6)
                        })
                    )
                }
                // boom
                if (projectile.isBoom) {
                    console.log('===>', myArr[enemy.counter % myArr.length])
                    updateProBar(enemy.counter % 3 === 0)
                    updateScore(500)
                    setTimeout(() => {
                        enemies.splice(index, 1)
                    }, 0)
                } else {
                    if (enemy.radius >= enemyRadiusMin) {
                        //increase our score
                        updateScore(100)
                        gsap.to(enemy, {
                            radius: enemy.radius - shootStep
                        })
                        setTimeout(() => {
                            projectiles.splice(projectileIndex, 1)
                        }, 0)
                    } else {
                        //increase our score + bonus
                        console.log('===>', myArr[enemy.counter % myArr.length])
                        updateProBar(enemy.counter % 3 === 0)
                        updateScore(500)
                        setTimeout(() => {
                            enemies.splice(index, 1)
                            projectiles.splice(projectileIndex, 1)
                        }, 0)
                    }
                }
            }
        })
    })
}

const shoot = (evt) => {
    if (!boomed) {
        const audio = new Audio();
        audio.src = './laser-gun.mp3'
        audio.play();
        const angle = Math.atan2(evt.clientY - canvas.height / 2,
            evt.clientX - canvas.width / 2)
        const velocity = {
            x: Math.cos(angle) * 10,
            y: Math.sin(angle) * 10
        }
        projectiles.push(
            new Projectile(canvas.width / 2, canvas.height / 2, 5, 'yellow', velocity)
        )
    }
    boomed = false
}

const boom = (evt) => {
    boomTimerID = setTimeout(() => {
        // user control boom direction
        // let angle = Math.atan2(evt.clientY - canvas.height / 2,
        //     evt.clientX - canvas.width / 2)
        // const velocity = {
        //     x: Math.cos(angle) * 20,
        //     y: Math.sin(angle) * 20
        // }
        // projectiles.push(
        //     new Projectile(canvas.width / 2, canvas.height / 2, 10, 'red', velocity, true)
        // )
        // booms at 8 fixed directions, bugs exist to remove boom
        const velocities = []
        for (let i = 0; i <= 360; i += 10) {
            velocities.push({
                x: Math.cos(i * (Math.PI / 180)) * 20,
                y: Math.sin(i * (Math.PI / 180)) * 20
            })
        }

        velocities.forEach((velocity, index) => {
            setTimeout(() => {
                projectiles.push(
                    new Projectile(canvas.width / 2, canvas.height / 2, 10, `hsl(${index * 10}, 100%, 50%)`, velocity, true)
                )
            }, 50 * index)
        })
        boomed = true
        playMusic(backgroundMusic)
    }, 500)
}

const enableClick = () => {
    addEventListener('click', shoot)
    if ('PointerEvent' in window) {
        addEventListener('pointerdown', boom);
        addEventListener('pointerup', () => {
            clearTimeout(boomTimerID)
        })
    } else {
        addEventListener('mousedown', boom);
        addEventListener('mouseup', () => {
            clearTimeout(boomTimerID)
        })
    }
}

function play() {
    init()
    spawnEnemies()
    animate()
    panel.style.display = 'none'
    setTimeout(enableClick, 50)
}

startGameBtn.addEventListener('click', play)

document.addEventListener('visibilitychange', function () {
    if (document.hidden) {
        clearInterval(spawnID);
        clearInterval(counter2ID)
    } else {
        spawnEnemies();
    }
}, false);

// play()